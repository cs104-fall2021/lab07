import math


def get_distance(p1, p2):
    distance_vector = []
    for i in range(len(p1)):
        distance_vector.append(p2[i] - p1[i])
    return distance_vector


def get_magnitude(vector):
    return math.sqrt(vector[0] ** 2 + vector[1] ** 2 + vector[2] ** 2)


def get_unit_vector(vector):
    magnitude = get_magnitude(vector)
    return [entry / magnitude for entry in vector]


def get_addition(vector1, vector2):
    return [vector1[i] + vector2[i] for i in range(3)]


def get_cosine_of_angle(vector1, vector2):
    magnitude1 = get_magnitude(vector1)
    magnitude2 = get_magnitude(vector2)
    value = 0
    for i in range(3):
        value += vector1[i] * vector2[i]
    return value / (magnitude2 * magnitude1)


p1 = (1, 4, 5)
p2 = (0, 4, 5)

# expect [-1, 0, 0]
print(get_distance(p1, p2))
my_vector = [0, -3, 4]
# unit vector [0, -0.6, 0.8]
print(get_unit_vector(my_vector))
print(get_magnitude(get_unit_vector(my_vector)))
