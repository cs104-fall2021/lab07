def check_if_anagram(word1, word2):
    a = list(word1)
    a = [c for c in word1]
    print(a)
    a.sort()
    print(a)
    b = list(word2)
    print(b)
    b.sort()
    print(b)
    return a == b


# Expect True
print(check_if_anagram("life", "file"))
# Expect True
print(check_if_anagram("ala", "laa"))
# Expect False
print(check_if_anagram("life", "line"))
# Expect False
print(check_if_anagram("alarm", "laark"))
