import math


def get_vector(point1, point2):
    return [point2[i] - point1[i] for i in range(3)]


point1 = (0, 1, 2)
point2 = (0, 2, 5)


# expect [0, 1, 3]
# print(get_vector(point1, point2))


def get_magnitude(vector):
    total = 0
    for i in range(3):
        total += vector[i] * vector[i]
    return math.sqrt(total)


def get_magnitude_alternative(vector):
    return math.sqrt(sum([vector[i] ** 2 for i in range(3)]))


my_vector = [0, -3, 4]


# expect 5
# print(get_magnitude(my_vector))
# print(get_magnitude_alternative(my_vector))


# expect [0.0, -0.6, 0.8]
def get_unit_vector(vector):
    return [vector[i] / get_magnitude(vector) for i in range(3)]


# print(get_unit_vector(my_vector))


def add_two_vectors(vector1, vector2):
    return [vector1[i] + vector2[i] for i in range(3)]


vector1 = [9, 5, 11]
vector2 = [1, 15, 19]


# expect [10, 20, 30]
# print(add_two_vectors(vector1, vector2))


def get_multiplication(matrix, vector):
    my_result = []
    for i in range(len(my_matrix)):
        total = 0
        for j in range(len(my_matrix[i])):
            total += my_matrix[i][j] * vector[j]
        my_result.append(total)
    return my_result


my_matrix = [[1, 2], [3, 4]]
my_vector = [7, 4]
# expect [15, 37]
print(get_multiplication(my_matrix, my_vector))
print(get_multiplication_alternative(my_matrix, my_vector))
