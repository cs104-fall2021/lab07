def factorial(n):
    result = 1
    if n != 0:
        for i in range(1, n + 1):
            result *= i
    return result


def calculate_e(n):
    total = 0
    for i in range(n + 1):
        total += 1 / factorial(i)
    return total


def calculate_pi(n):
    total = 0
    for i in range(1, n + 1):
        total += (-1) ** (i + 1) * 4 / (2 * i - 1)
    return total


def calculate_pi_alternative(n):
    result = 1
    for i in range(1, n + 1):
        result *= (4 * i ** 2) / (4 * i ** 2 - 1)
    return 2 * result


for number in [10, 100, 1000, 10000]:
    print("pi is after", number, "iterations", calculate_pi(number))
for number in [10, 100, 1000, 10000]:
    print("pi is after", number, "iterations", calculate_pi_alternative(number))
for number in [10, 100, 1000, 10000]:
    print("e is after", number, "iterations", calculate_e(number))
